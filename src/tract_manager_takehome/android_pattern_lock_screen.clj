(ns tract-manager-takehome.android-pattern-lock-screen)

(def default-board
  (let [m [[1 2 3] [4 5 6] [7 8 9]]
        board-size (count m)]
    (apply merge (for [x (range board-size)
                       y (range board-size)
                       :let [value (nth (nth m x) y)]]
                   {value {:x x :y y :val value}}))))

(defn area-of-triangle [{x1 :x y1 :y} {x2 :x y2 :y} {x3 :x y3 :y}]
  (/ (+ (* x1 (- y2 y3))
        (* x2 (- y3 y1))
        (* x3 (- y1 y2)))
     2))

(defn collinear? [p1 p2 p3]
  (zero? (area-of-triangle p1 p2 p3)))

(defn monotonic? [{x1 :x y1 :y} {x2 :x y2 :y} {x3 :x y3 :y}]
  (or (and (<= x1 x2 x3) (<= y1 y2 y3))
      (and (>= x1 x2 x3) (>= y1 y2 y3))))

(defn in-between? [p1 p2 p3]
  (and (collinear? p1 p2 p3) (monotonic? p1 p2 p3)))

(defn valid-segment?
  ([p1 p2 visited] (valid-segment? p1 p2 visited default-board))
  ([p1 p2 visited board]
  (let [points (vals board)
        other-points (remove #{p1 p2} points)
        intermediate-points (filter #(in-between? p1 % p2) other-points)
        unvisited-intermediate-points (remove visited intermediate-points)]
    (empty? unvisited-intermediate-points))))

(defn valid-path?
  ([path] (valid-path? path default-board))
  ([path board]
  (loop [[x y & ys] path
         visited #{(get board x)}
         valid (contains? board x)]
    (let [point1 (get board x)
          point2 (get board y)]
      (cond
        (nil? y) valid
        (or (false? valid) (nil? point2) (contains? visited point2)) false
        :else (recur (cons y ys)
                     (conj visited point2)
                     (valid-segment? point1 point2 visited)))))))

(assert (true? (valid-path? [1])))
(assert (true? (valid-path? [1 6 7 4])))   ;; knights jump is valid
(assert (true? (valid-path? [2 1 3])))     ;; 2 is already used, so we can cross it
(assert (false? (valid-path? [1 3 2])))     ;; can't get from 1 to 3 without using 2 first
(assert (false? (valid-path? [1 9])))       ;; can't cross 5 without using
(assert (false? (valid-path? [0])))
(assert (false? (valid-path? [1 2 3 2 1]))) ;; can't use dots more than once
(assert (false? (valid-path? [0 1 2 3])))   ;; there's no dot 0
