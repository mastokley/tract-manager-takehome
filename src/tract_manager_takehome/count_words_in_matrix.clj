(ns tract-manager-takehome.count-words-in-matrix)

(defn cell-value [matrix {i :i j :j}] (get-in matrix [i j]))

(defn find-neighbors [matrix {i :i j :j}]
  (let [east-edge (count (first matrix))
        south-edge (count matrix)
        north (if-not (neg? (dec i))          {:i (dec i) :j j})
        east  (if-not (>= (inc j) east-edge)  {:i i       :j (inc j)})
        south (if-not (>= (inc i) south-edge) {:i (inc i) :j j})
        west  (if-not (neg? (dec j))          {:i i       :j (dec j)})]
    (remove (comp nil? :location)
            [{:direction :north :location north}
             {:direction :east  :location east}
             {:direction :south :location south}
             {:direction :west  :location west}])))

(defn find-neighbor [matrix location direction]
  (->> location
       (find-neighbors matrix)
       (filter #(= direction (:direction %)))
       first
       :location))

(defn enumerate-matrix [matrix]
  (for [i (range (count matrix))
        j (range (count (first matrix)))]
        {:i i :j j :value (get-in matrix [i j])}))

(defn values-by-location [matrix]
  (reduce
   (fn [acc {value :value :as cur}] (update acc value conj cur))
   {}
   (enumerate-matrix matrix)))

(defn find-word-in-direction
  [matrix word root-location direction]
  (loop [[c & cs] word
         location root-location]
    (cond (nil? c) word
          (not= c (cell-value matrix location)) nil
          :else (recur cs (find-neighbor matrix location direction)))))

(defn find-word [matrix word root-location]
  (let [neighbors (find-neighbors matrix root-location)
        directions-of-exploration (map :direction neighbors)]
    (keep (partial find-word-in-direction matrix word root-location)
          directions-of-exploration)))

(defn count-words-in-matrix [matrix word]
  (let [characters-by-location (values-by-location matrix)
        root-locations (get characters-by-location (first word))]
    (count (mapcat (partial find-word matrix word) root-locations))))

(let [mat [[\A \O \T \D \L \R \O \W]
           [\L \C \B \M \U \M \L \U]
           [\D \R \U \J \D \B \L \J]
           [\P \A \Z \H \Z \Z \E \F]
           [\B \C \Z \E \L \F \H \W]
           [\R \K \U \L \V \P \P \G]
           [\A \L \B \L \P \O \P \Q]
           [\B \E \M \O \P \P \J \Y]]]
  (assert (zero? (count-words-in-matrix mat "")))
  (assert (= 2 (count-words-in-matrix mat "HELLO")))
  (assert (= 1 (count-words-in-matrix mat "WORLD")))
  (assert (= 2 (count-words-in-matrix mat "BUZZ")))
  (assert (zero? (count-words-in-matrix mat "CLOJURE")))
  (assert (zero? (count-words-in-matrix mat "COWABUNGA"))))
